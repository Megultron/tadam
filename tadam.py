import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from resnet import resnet12

class TADAM(nn.Module):
    def __init__(self, feature_extractor):
        super(TADAM, self).__init__()

        self.feature_extractor = feature_extractor
        self.alpha = nn.Parameter(torch.tensor([1], dtype=torch.float,
            requires_grad=True))

    def forward(self, batch):
