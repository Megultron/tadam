import torch
import torch.nn as nn
import torch.nn.functional as F

def conv3x3(in_planes, out_planes, stride=1):
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                        padding=1, bias=False)

class ResidualFCNet(nn.Module):
    def __init__(self, in_feats, target_feats, activation=F.elu):
        super(ResidualFCNet, self).__init__()

        self.fc1 = nn.Linear(in_feats, target_feats)
        self.fc2 = nn.Linear(target_feats, target_feats)
        self.fc3 = nn.Linear(target_feats, target_feats)

    def forward(self, x):
        x1 = self.fc1(x)
        x2 = self.fc2(x1 + x)
        x3 = self.fc3(x2 + x)

        return x3

class TEN(nn.Module):
    def __init__(self, n_filters, task_embedding_dim=512, activation=F.elu):
        super(TEN, self).__init__()
        
        self.beta0 = nn.Parameter(torch.zeros([n_filters], dtype=torch.float,
            requires_grad=True))
        self.gamma0 = nn.Parameter(torch.zeros([n_filters], dtype=torch.float,
            requires_grad=True))

        self.beta_fc_net = ResidualFCNet(task_embedding_dim, n_filters)
        self.gamma_fc_net = ResidualFCNet(task_embedding_dim, n_filters)

    def forward(self, task_embedding):
        pre_beta = self.beta_fc_net(task_embedding)
        beta = pre_beta * self.beta0

        pre_gamma = self.gamma_fc_net(task_embedding)
        gamma = pre_gamma * self.gamma0 + 1.

        return gamma, beta

class Block12(nn.Module):
    def __init__(self, in_planes, planes, stride=1, activation=F.elu, downsample=None):
        super(Block12, self).__init__()
        
        self.conv_residual = conv3x3(in_planes, planes, stride=stride)
        self.bn_residual = nn.BatchNorm2d(planes)
        
        self.conv1 = conv3x3(in_planes, planes, stride=stride)
        self.bn1 = nn.BatchNorm2d(planes)
        self.ten1 = TEN(planes)

        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.ten2 = TEN(planes)

        self.conv3 = conv3x3(planes, planes)
        self.bn3 = nn.BatchNorm2d(planes)
        self.ten3 = TEN(planes)

        self.pool = nn.MaxPool2d(kernel_size=2, stride=2, padding=1)
        self.activation = activation

    def forward(self, x, task_embedding=None):
        residual = self.conv_residual(x)
        residual = self.bn_residual(residual)

        x = self.conv1(x)
        x = self.bn1(x)
        if task_embedding is not None:
            gamma, beta = self.ten1(task_embedding)
            x = (x * gamma) + beta
        x = self.activation(x)

        x = self.conv2(x)
        x = self.bn2(x)
        if task_embedding is not None:
            gamma, beta = self.ten2(task_embedding)
            x = (x* gamma) + beta
        x = self.activation(x)

        x = self.conv3(x)
        x = self.bn3(x)
        if task_embedding is not None:
            gamma, beta = self.ten3(task_embedding)
        
        x = self.activation(x + residual)

        return self.pool(x)

class ResNet(nn.Module):
    def __init__(self, block, layers, activation=F.elu):
        super(ResNet, self).__init__()

        self.in_planes = 64
        self.activation = activation 

        self.max_pool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.conv1 = nn.Conv2d(3, self.in_planes, kernel_size=7, stride=2, padding=3,
                bias=False)
        self.bn1 = nn.BatchNorm2d(self.in_planes)
        self.block1 = self._make_block(block, 64, layers[0])
        self.block2 = self._make_block(block, 64*2, layers[1], stride=2)
        self.block3 = self._make_block(block, 64*4, layers[2], stride=2)
        self.block4 = self._make_block(block, 64*8, layers[3], stride=2)
        #self.avg_pool = nn.AvgPool2d(7, stride=1)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal(m.weight, mode='fan_out',
                        nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_block(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.in_planes != planes:
            downsample = nn.Sequential(
                        nn.Conv2d(self.in_planes, planes, kernel_size=1,
                            stride=stride, bias=False),
                    )

        layers = []
        layers.append(block(self.in_planes, planes, stride, downsample=downsample))
        for i in range(1, blocks):
            layers.append(block(self.in_planes, planes))

        self.in_planes = planes

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activation(x)
        x = self.max_pool(x)

        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.block4(x)

        #x = self.avg_pool(x)
        x = x.view(x.size(0), -1)

        return x

def resnet12(**kwargs):
    model = ResNet(Block12, [1,1,1,1], **kwargs)
    return model
